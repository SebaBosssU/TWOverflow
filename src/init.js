require([
    'two/locale'
], function (
    Locale
) {
    Locale.create('common', __overflow_locales, 'en')
})
